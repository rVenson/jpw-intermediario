const mongoose = require('../data');

let alunoSchema = new mongoose.Schema(
  {
    documento: {
      type: String,
      required: true,
    },
    nome: {
      type: String,
      required: true,
    },
    telefone: String,
    email: String,
    matricula: {
      type: String,
      required: true,
    },
    disciplina: String,
  },
  { timestamps: true }
);

let Aluno = mongoose.model('Aluno', alunoSchema);
module.exports = Aluno;
