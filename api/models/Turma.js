const { Schema } = require('../data');
const mongoose = require('../data');

let turmaSchema = new mongoose.Schema(
  {
    nome: {
      type: String,
      required: true,
    },
    professor: {
      id: {
        type: Schema.Types.ObjectId,
        ref: 'professor',
      },
      nome: String
    },
    aluno: {
      id: {
        type: Schema.Types.ObjectId,
        ref: 'aluno',
      },
      nome: String
    },
  },
  { timestamps: true }
);

let Turma = mongoose.model('Turma', turmaSchema);
module.exports = Turma;
