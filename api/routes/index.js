const express = require('express')
const router = express.Router()
const alunos = require('./alunos')
const profs = require('./profs')
const turmas = require('./turmas')

router.use(express.json())
router.use('/alunos', alunos)
router.use('/professores', profs)
router.use('/turmas', turmas)

module.exports = router