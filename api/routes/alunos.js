const express = require('express');
const router = express.Router();
const Aluno = require('../models/Aluno');

// mostrar todos os alunos
router.get('/', async (req, res) => {
  let nLimit = parseInt(req.query.limit) || 10;
  let nSkip = parseInt(req.query.skip) || 0;
  const alunos = await Aluno.find().limit(nLimit).skip(nSkip);
  res.json(alunos);
});

// mostrar um aluno
router.get('/:id', async (req, res, next) => {
  try {
    const id = req.params.id;
    var alunos = await Aluno.findById(id);
    if (!alunos)
      return res.status(404).json({
        erro: 'Aluno não encontrado',
      });
    res.json(alunos);
  } catch (err) {
    next(err);
  }
});

// Inserir um novo aluno
router.post('/', async (req, res) => {
  const aluno = new Aluno(req.body);
  var resultado = await aluno.save();
  return res.json(aluno);
});

// Atualizar um aluno
router.put('/:id', async (req, res) => {
  const id = req.params.id;
  const novoAluno = req.body;
  const atualAluno = await Aluno.findByIdAndUpdate(id, novoAluno, {
    new: true,
  });
  return res.json(atualAluno);
});

// Deletar aluno
router.delete('/:id', async (req, res) => {
  const id = req.params.id;
  const aluno = await Aluno.findByIdAndDelete(id);
  res.json(aluno);
});

module.exports = router;
