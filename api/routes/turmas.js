const express = require('express');
const router = express.Router();
const Aluno = require('../models/Turma');

// mostrar todas as turmas
router.get('/', async (req, res) => {
  let nLimit = parseInt(req.query.limit) || 10;
  let nSkip = parseInt(req.query.skip) || 0;
  const turmas = await Turma.find().limit(nLimit).skip(nSkip);
  res.json(turmas);
});

// mostrar uma turma
router.get('/:id', async (req, res, next) => {
  try {
    const id = req.params.id;
    var turmas = await Turma.findById(id);
    if (!turmas)
      return res.status(404).json({
        erro: 'Turma não encontrado',
      });
    res.json(turmas);
  } catch (err) {
    next(err);
  }
});

// Inserir uma nova turma
router.post('/', async (req, res) => {
  const turma = new Turma(req.body);
  var resultado = await turma.save();
  return res.json(turma);
});

// Atualizar um turma
router.put('/:id', async (req, res) => {
  const id = req.params.id;
  const novaTurma = req.body;
  const atualTurma = await Turma.findByIdAndUpdate(id, novoTurma, {
    new: true,
  });
  return res.json(atualTurma);
});

// Deletar turma
router.delete('/:id', async (req, res) => {
  const id = req.params.id;
  const turma = await Turma.findByIdAndDelete(id);
  res.json(turma);
});

module.exports = router;
