const mongoose = require('mongoose');

mongoose.connection.on('connected', function () {
  console.log('Conexão com o MongoDB realizada com sucesso');
});

mongoose.connection.on('disconnected', function () {
  console.log('Desconectando o banco!!');
});

module.exports = mongoose;
